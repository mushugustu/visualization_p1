## Introduction

This practice uses data from
the http://archive.ics.uci.edu/ml/ UC Irvine Machine
Learning Repository. We will be using the "Individual household
electric power consumption Data Set":

* Dataset https://d396qusza40orc.cloudfront.net/exdata%2Fdata%2Fhousehold_power_consumption.zip Electric power consumption[20Mb]

* Description: Measurements of electric power consumption in
one household with a one-minute sampling rate over a period of almost
4 years. Different electrical quantities and some sub-metering values
are available.


The  descriptions of the 9 variables in the dataset are taken
from
the https://archive.ics.uci.edu/ml/datasets/Individual+household+electric+power+consumption UCI
web site:


Date: Date in format dd/mm/yyyy
Time: time in format hh:mm:ss 
Global_active_power: household global minute-averaged active power (in kilowatt) 
Global_reactive_power: household global minute-averaged reactive power (in kilowatt) 
Voltage: minute-averaged voltage (in volt) 
Global_intensity: household global minute-averaged current intensity (in ampere) 
Sub_metering_1: energy sub-metering No. 1 (in watt-hour of active energy). It corresponds to the kitchen, containing mainly a dishwasher, an oven and a microwave (hot plates are not electric but gas powered). 
Sub_metering_2: energy sub-metering No. 2 (in watt-hour of active energy). It corresponds to the laundry room, containing a washing-machine, a tumble-drier, a refrigerator and a light. 
Sub_metering_3: energy sub-metering No. 3 (in watt-hour of active energy). It corresponds to an electric water-heater and an air-conditioner.


## Loading the data


* The dataset has 2,075,259 rows and 9 columns.

* We will only be using data from the dates 2007-02-01 and
2007-02-02. One alternative is to read the data from just those dates
rather than reading in the entire dataset and subsetting to those
dates.

* It may be useful to convert the Date and Time variables to
Date/Time classes in R using the `strptime()` and `as.Date()`
functions.

* In this dataset missing values are coded as `?`.


## Making Plots

The goal here is to examine how household energy usage
varies over a 2-day period in February, 2007. The task is to
reconstruct the following plots below, all of which were constructed
using the base plotting system.

For each plot you should

* Construct the plot and save it to a PNG file with a width of 480
pixels and a height of 480 pixels.

* Name each of the plot files as `plot1.png`, `plot2.png`, etc.

* Create a separate R code file (`plot1.R`, `plot2.R`, etc.) that
constructs the corresponding plot, i.e. code in `plot1.R` constructs
the `plot1.png` plot. Your code file **should include code for reading
the data** so that the plot can be fully reproduced. You should also
include the code that creates the PNG file.


The four plots that you will need to construct are shown below.


### Plot 1


![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2.png)


### Plot 2

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3.png)


### Plot 3

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-4.png)


### Plot 4

![plot of chunk unnamed-chunk-5](figure/unnamed-chunk-5.png)
